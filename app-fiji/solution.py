from album.runner.api import setup

global ImageJ, ij
from io import StringIO

env_file = StringIO("""name: fiji
channels:
  - conda-forge
  - defaults
dependencies:
  - pyyaml
  - python=3.6
  - scyjava
  - openjdk=8.0.152
""")


def init_ij():
    global ImageJ
    from scyjava import config, jimport
    config.add_repositories({'scijava.public': 'https://maven.scijava.org/content/groups/public'})
    config.add_endpoints('sc.fiji:fiji:2.1.1')
    ImageJ = jimport("net.imagej.ImageJ")


def album_install():
    init_ij()


def album_run():
    global ImageJ, ij
    init_ij()
    ij = ImageJ()
    print("Starting Fiji..")
    ij.launch()


def album_close():
    global ij
    # ij.dispose()


setup(
    group="ida-mdc",
    name="app-fiji",
    version="0.1.0-SNAPSHOT",
    title="Fiji",
    description="Fiji is just ImageJ",
    authors=["Deborah Schmidt"],
    cite=[{"text": "Schindelin, J.; Arganda-Carreras, I. & Frise, E. et al. (2012), \"Fiji: an open-source platform for "
        "biological-image analysis\", Nature methods 9(7): 676-682, PMID 22743772", "doi": "10.1038/nmeth.2019", "url": "https://fiji.sc"}],
    tags=["fiji", "imagej", "app"],
    license="MIT",
    documentation=[],
    covers=[{
        "description": "Dummy cover image.",
        "source": "cover.png"
    }],
    album_api_version="0.1.1",
    args=[],
    install=album_install,
    run=album_run,
    close=album_close,
    dependencies={'environment_file': env_file}
)
