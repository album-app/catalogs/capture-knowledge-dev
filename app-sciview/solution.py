from album.runner import setup

global ImageJ, ij

from io import StringIO

env_file = StringIO("""name: sciview-album
channels:
  - conda-forge
  - defaults
dependencies:
  - pyyaml
  - python=3.6
  - scyjava
  - openjdk=8.0.152
""")

global sciview


def init_sciview():
    global SciView
    from scyjava import config, jimport
    config.add_repositories(
        {'scijava.public': 'https://maven.scijava.org/content/groups/public'})
    config.add_endpoints('sc.iview:sciview:571391c')
    SciView = jimport("sc.iview.SciView")


def install():
    init_sciview()


def run():
    global SciView, sciview
    init_sciview()
    print("Starting sciview..")
    sciview = SciView.create()


def close():
    global sciview
    from scyjava import jimport

    # input('Press any key to exit')
    import time
    time.sleep(30)
    # while not sciview.isClosed:
    #     time.sleep(1)

    sciview.close()
    System = jimport('java.lang.System')
    System.exit(0)


setup(
    group="ida-mdc",
    name="sciview",
    version="0.1.0",
    title="sciview",
    description="sciview visualization on scijava",
    authors=["Kyle Harrington"],
    cite=[{
        "text":
        "Günther, U., and Harrington, K.I., 2020. Tales from the Trenches: Developing a new 3D viewer for the ImageJ community. In VisGap/Eurographics and Eurovis.",
        "doi": ""
    }],
    tags=["sciview", "imagej", "app"],
    license="MIT",
    documentation=[],
    covers=[{
        "description": "Dummy cover image.",
        "source": "cover.png"
    }],
    album_api_version="0.1.1",
    args=[],
    install=install,
    run=run,
    close=close,
    dependencies={'environment_file': env_file}
)
